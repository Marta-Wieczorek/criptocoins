import React, {Component} from 'react';


const TickerContext = React.createContext();
export const TickerConsumer = TickerContext.Consumer;


export class TickerProvider extends Component {
    state = {
        tickers: null,
        fetching: false,
        error: null,
        tickerIds: null,
    }


    componentDidMount() {
        this.setState({
            fetching: true,
            error: null
        })

        fetch(
            process.env.PUBLIC_URL + '/tickers.json'
        ).then(
            response => response.json()
        ).then(
            tickers => this.setState({
                fetching: false,
                tickers: tickers,
                tickerIds: Object.keys(tickers)
            })
        ).catch(
            error => this.setState({
                error,
                fetching: false,
            })
        )
    }

    render() {
        return (
            <TickerContext.Provider value={this.state}>
                {this.props.children}
            </TickerContext.Provider>
        )
    }
}

export function withTicker(Component) {
    function TickerAwareComponent(props) {
        return (
            <TickerContext.Consumer>
                {
                    state => <Component {...props} {...state}/>
                }
            </TickerContext.Consumer>
        )
    }

    TickerAwareComponent.displayName = `TickerAware(${Component.displayName || Component.name || 'Component'})`
    return TickerAwareComponent
}
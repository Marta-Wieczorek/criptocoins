import React, { Component} from 'react';
import { Link } from 'react-router-dom'
import { withTicker } from "./Context";
import * as Icon from 'react-cryptocoins';

function getIcon(arg) {
    switch (arg){
        case "Eth": return <Icon.Eth/>
        case "Btc": return <Icon.Btc />
        case "Usd": return <i className="fas fa-dollar-sign iconFont"></i>
        case "Xmr": return <Icon.Xmr />
        case "Zec": return <Icon.Zec />
        case "Ltc": return <Icon.Ltc />
        case "Etc": return <Icon.Etc />
        case "Dsh": return <Icon.Dash />
        case "Rrt": return <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 2000 2000"><g fill="#000000"><path d="m1400.78 966.69c64.71-64.7 97.5-143.58 97.5-234.44 0-89.63-32.72-168.16-97.3-233.43-64.79-65.38-135.41-98.54-209.92-98.54h-656.46v169h656.47c28.27 0 58.65 16.23 90.27 48.22 32.68 33.1 47.9 69.54 47.9 114.72 0 46.13-15.13 82.76-47.64 115.27-31.62 31.62-62.09 47.65-90.53 47.65h-650.84v703.94h169v-534.86h298l318.94 534.89h186.14l-319-534.9c73.69-.69 143.42-33.49 207.47-97.52"/><path d="M1000,2000C448.6,2000,0,1551.32,0,999.82,0,448.52,448.6,0,1000,0S2000,448.52,2000,999.82C2000,1551.32,1551.4,2000,1000,2000ZM170.82,795.83a855.3,855.3,0,0,0-24.69,204c0,470.91,383.06,854,853.87,854s853.85-383.11,853.85-854c0-470.74-383-853.72-853.85-853.72-165,0-325,47-462.8,135.85C399.68,370.65,290.95,496.74,222.73,646.62l-10.39,22.84h941.79c24.67,0,82.06,5.56,82.06,57s-57.39,57.05-82.06,57.05H173.85Z"/></g></svg>
        default: console.log('Sorry, we are out of icons.')
    }
}

function addSlash (arg) {
    if (typeof arg === "string") {
        let stringFirst= arg.substr(0,3)
        let stringSecond = arg.substr(3)
        return stringFirst + '/' + stringSecond
    }
    return null
}


function getFirstIcon (arg) {
    if (typeof arg === "string") {
        let stringFirst = arg.substr(0,3)
        let firstLetter = stringFirst.charAt(0)
        return getIcon(firstLetter.toUpperCase() + stringFirst.substr(1).toLowerCase())
        }

    return null
}

function getLastIcon (arg) {
    if (typeof arg === "string") {
        let stringLast = arg.substr(3)
        let firstLetter = stringLast.charAt(0)
        return getIcon(firstLetter.toUpperCase() + stringLast.substr(1).toLowerCase())
    }
    return null
}

class List extends Component {

    render() {

        return (
            this.props.tickerIds !== null ? (
                <div className="Ticker container ">
                    <p>Choose currency pairing from list below</p>
                    <div className="tickerDiv container">

                        <ul>
                            {
                                this.props.tickerIds.map(
                                    tickerId => (
                                        <Link to={`/tickers/${tickerId}`} className='Links'>
                                        <li className="elementLi">
                                            <div className="element">
                                                {getFirstIcon(tickerId)} {addSlash(tickerId)} {getLastIcon(tickerId)}
                                            </div>
                                        </li>
                                        </Link>
                                    )
                                )
                            }
                        </ul>
                    </div>
                </div>
            ) : null
        )
    }

}

export default withTicker(List)
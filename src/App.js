import React, { Component } from 'react';
import './App.css';
import {
    BrowserRouter as Router,
    Route,
} from 'react-router-dom';
import List from "./List";
import { TickerProvider, withTicker } from "./Context";
import SingleAssets from "./SingleAssets";

class App extends Component {
    render() {
        return (
            <TickerProvider>
                <div className="App container-fluid">
                    <Router>
                        <div className="container">
                            <h1>Cryptocoins</h1>
                            <Route exact path={`/tickers/:tickerId`} component={SingleAssets} />
                            <Route exact path="/" component={List}/>
                        </div>
                    </Router>
                </div>
            </TickerProvider>
        );
    }
}

export default withTicker(App);

## Objective
The goal was to build a SPA that displays exchange rates for cryptocurrencies.

## Start

These instructions will allow you to copy the project and run it on your computer in order to develop or change applications, as well as for testing.

### Requirements


A few things are needed to run the project
```

-node.js
-npm oraz yarn(mainly yarn used for the project)
-React.js
-React-Router-Dom
-React-cryptocoins


```

### Installation

In case you already have the node, npm, yarn and the create-react-app package installed, just clone the repository and fill the packages with the command:

```
yarn
```
Otherwise :

First you need a tool to run node.js, you will install it using node version manager (* [NVM] (https://github.com/creationix/nvm))

```
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm

```


Restart the terminal and check whether the utility has installed correctly:
```
command -v nvm

```
Install the node using:
```
nvm install node

```

The next step is to download the Yarn package management tool, for this you first need to add the repository key to Ubuntu.
* [yarn] (https://yarnpkg.com/lang/en/docs/install/#debian-stable)

```
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list

```

Now, just refresh the list of our repositories and install Yarn:
```
sudo apt-get update && sudo apt-get install yarn
```

Another thing is to install the React.js library using the create-react-app. Detailed installation instructions in the link.
* [create-react-app] (setup.md)


To support the same methods, you need the following packages:

```
-React-router-DOM
-React-cryptoicon

```

We intend successively:

```
yarn add react-router-dom
yarn add react-cryptoicon

```

After the entire configuration, to run the application you must use the command

```
yarn start
```

In the case of problems with the version of packages after cloning, you can delete the file [yarn.lock] (yarn.lock)


## Built using:

* [NVM] (https://github.com/creationix/nvm)
* [yarn] (https://yarnpkg.com/lang/en/docs/install/#debian-stable)
* [create-react-app] (setup.md)
* [react-router-dom] (https://www.npmjs.com/package/react-router-dom)
* [react-cryptocoins] (https://github.com/kirillshevch/react-cryptocoins)


## Controversial decisions


When coding my application, I had to decide several times which solution will not destroy the order of the application, and will enable the implementation of the intended purpose. Below are some examples:

```
- Implementing the RRT icon as an svg file.
- Impelemnting the USD icon as the font icon.
- Save the state in localStorage or don't.
- Transfer of props.
```

### RRT icon:

Due to the lack of the RRT icon in the react-cryptocoins set, I had to find a replacement. I was surprised that Recovery Right Tokens is not a popular currency and its icon is rarely used. Therefore, after finding it on the site: https://coinranking.com/coin/recoveryrighttokens-rrt in svg format, I copied its code and processed it to the desired shape.

### USD icon:


Like the RRT icon, USD is not included in the react-cryptoicons set, so I decided to find the corresponding icon on the page: https://fontawesome.com and insert it as a font element.

### localStorage:

By using Router to give appropriate paths for components and Links
  when defining the buttons, I did not have to save the application state in localStorage.
### Props transfer


I decided to use the context together with a higher order component (HOC), thanks to which the transfer of status and downloading of props by components was more legible and easier.

## Author
**Marta Wieczorek**


